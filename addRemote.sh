#!/bin/sh

git remote add -f python_scripts https://gitlab.com/quocsangnguyen/python_scripts.git
#git read-tree --prefix=python_scripts/ -u python_scripts/master

git remote add -f testingRepo https://gitlab.com/quocsangnguyen/testingRepo.git
#git read-tree --prefix=testingRepo/ -u testingRepo/master

echo '-----Current remote-----'
git remote -v
echo '----------'

#mkdir python_scripts
#mkdir testingRepo

git subtree add --prefix=python_scripts https://gitlab.com/quocsangnguyen/python_scripts.git master --squash
git subtree add --prefix=testingRepo https://gitlab.com/quocsangnguyen/testingRepo.git temp1 --squash
